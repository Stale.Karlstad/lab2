package INF101.lab2;

//import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
   
    int capacity=20;
    ArrayList<FridgeItem> fridgeList= new ArrayList<>(20);
    
    @Override
    public int nItemsInFridge() {       
        int stock=fridgeList.size();
        return stock;
    }

    @Override
    public int totalSize() {       
        return capacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        
        int capacity=totalSize();        
        FridgeItem newItem = item;
        int size = fridgeList.size();
        if (size<capacity){
            fridgeList.add(newItem);
            
            return true;
        }else{            
            return false;
        }        
    }

    @Override
    public void takeOut(FridgeItem item) {
        FridgeItem newItem = item;

        if (fridgeList.contains(newItem)){
            fridgeList.remove(newItem);
        }
        else{
            throw new NoSuchElementException();
        }                
    }

    @Override
    public void emptyFridge() {        
        fridgeList.clear();       
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {        
        ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        
        for (FridgeItem item: fridgeList){
            if (item.hasExpired()){
                expiredItems.add(item);
            }        
        }
        for (FridgeItem item: expiredItems){
            fridgeList.remove(item);
        }        
    return expiredItems;
    }    
}
